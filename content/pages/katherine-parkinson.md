+++
title = "Katherine Parkinson"
actors = ["Katherine Parkinson"]

shows = [
  "The IT Crowd",
  "Extras"
]
+++


Katherine Parkinson is a proper actor. She's also really good at being funny.
