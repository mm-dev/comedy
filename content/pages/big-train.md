+++
title = "Big Train"
shows = ["Big Train"]

writers = [
  "Graham Linehan",
  "Simon Pegg",
  "Kevin Eldon",
  "Arthur Mathews",
  "Amelia Bullmore",
  "Rebecca Front",
  "Christopher Morris",
  "Julia Davis",
  "Adam Buxton",
  "Joe Cornish",
  "Robert Webb",
  "Catherine Tate",
  "Mark Heap"
]
directors = [
  "Graham Linehan",
  "Christopher Morris"
]
actors = [
  "Mark Heap",
  "Catherine Tate",
  "Amelia Bullmore",
  "Rebecca Front",
  "Julia Davis",
  "Phil Cornwell",
  "Simon Pegg",
  "Kevin Eldon"
]
+++


Absolutely seminal and not in a rude way. Some of the best talent in British Comedy was involved in this series. It's still hilarious, and original today.
