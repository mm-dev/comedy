+++
title = "Alice Lowe"
actors = ["Alice Lowe"]

shows = [
  "Garth Merenghi's Darkplace",
  "The IT Crowd",
  "Black Books",
  "The Mighty Boosh"
]
+++

> You've got some shit on your head.

Sometimes overlooked --- big mistake! Alice Lowe has quite the on-screen presence. Her menstrual levitations in the 'Hell Hath Fury' episode of [Garth Merenghi's Darkplace](/pages/garth-merenghis-darkplace) will never leave me.
