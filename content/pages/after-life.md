+++
title = "After Life"
shows = ["After Life"]

producers = ["Charlie Hanson"]
writers = ["Ricky Gervais"]
directors = ["Ricky Gervais"]
actors = [
  "Ricky Gervais",
  "Ashley Jensen",
  "Karl Pilkington"
]
+++


After Life is a lovely sitcom.
