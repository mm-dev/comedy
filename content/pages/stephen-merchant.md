+++
title = "Stephen Merchant"
writers = ["Stephen Merchant"]
directors = ["Stephen Merchant"]
actors = ["Stephen Merchant"]

shows = [
  "The Office",
  "Extras",
  "The Ricky Gervais Show",
  "The Ricky Gervais Show (Radio)",
  "Outlaws"
]
+++


> Look after the pennies and the pounds will look after themselves.

The tall man from the West Country. For those of you who don't know, Steve is 6' 7" tall. And just to be clear, he doesn't carry it well at all.

