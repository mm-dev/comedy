+++
title = "Ricky Gervais"
writers = ["Ricky Gervais"]
directors = ["Ricky Gervais"]
actors = ["Ricky Gervais"]

shows = [
  "The Office",
  "The Ricky Gervais Show",
  "The Ricky Gervais Show (Radio)",
  "Extras",
  "Derek"
]
+++


[Ricky Gervais](/pages/ricky-gervais) is a comedian, writer, actor and director.
