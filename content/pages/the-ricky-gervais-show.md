+++
title = "The Ricky Gervais Show"
shows = ["The Ricky Gervais Show"]

writers = [
  "Stephen Merchant",
  "Ricky Gervais",
  "Karl Pilkington"
]
actors = [
  "Stephen Merchant",
  "Ricky Gervais",
  "Karl Pilkington"
]
+++


The XFM shows beget the podcasts. The podcasts beget The Ricky Gervais Show.
