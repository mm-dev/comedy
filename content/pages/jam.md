+++
title = "Jam"
shows = ["Jam"]

writers = [
  "Graham Linehan",
  "Peter Baynham",
  "Kevin Eldon",
  "Arthur Mathews",
  "Amelia Bullmore",
  "David Quantick",
  "Christopher Morris",
  "Mark Heap"
]
directors = [
  "Christopher Morris"
]
actors = [
  "Christopher Morris",
  "Amelia Bullmore",
  "Julia Davis",
  "Mark Heap",
  "Kevin Eldon"
]
+++


One of the darkest, weirdest things you'll ever see. [Chris Morris](/pages/christoper-morris) in full effect.

This series can be hard to come by but it's well worth hunting down. It's full of creepy and bizarre characters, and even the sound design is notably twisted.

Watching this, I often find myself wondering if it's ok to laugh or if that's... a bad sign.
