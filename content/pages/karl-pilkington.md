+++
title = "Karl Pilkington"
writers = ["Karl Pilkington"]
actors = ["Karl Pilkington"]

shows = [
  "Derek",
  "Sick of It",
  "The Ricky Gervais Show (Radio)",
  "The Ricky Gervais Show"
]
+++


> Babies are comin' out mardy. They are, they're comin' out all demicky.

Head like an orange. Play a record.
