+++
title = "Man Down"
shows = ["Man Down"]

producers = ["Charlie Hanson"]
writers = ["Greg Davies"]
directors = ["Al Campbell"]
actors = [
  "Greg Davies",
  "Rik Mayall",
  "Stephanie Cole",
  "Isy Suttie",
  "Tony Robinson",
  "Roisin Conaty"
]
+++


Man Down is a lovely sitcom.
