+++
title = "The Office"
shows = ["The Office"]

writers = [
  "Stephen Merchant",
  "Ricky Gervais"
]
directors = [
  "Stephen Merchant",
  "Ricky Gervais"
]
actors = [
  "Stephen Merchant",
  "Ricky Gervais",
  "Matthew Holness",
  "Martin Freeman"
]
+++


> "If you want the rainbow, you've got to put up with the rain." Do you know who said that? Dolly Parton. 

> And people say she's just a big pair of tits.

The original and best. It doesn't matter how many times I watch this, I can put it on any time and am guaranteed laughs.

{{< spoiler >}}
Although it's all a bit obvious, the finale in the Christmas special is magnificently satisfying. David meets a woman who actually likes him, Finchy finally gets told to fuck off and Dawn gets rid of the dickhead and gets with Tim.

It's heartwarming.
{{< /spoiler >}}
