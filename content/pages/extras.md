+++
title = "Extras"
shows = ["Extras"]

producers = ["Charlie Hanson"]
writers = [
  "Stephen Merchant",
  "Ricky Gervais"
]
directors = [
  "Stephen Merchant",
  "Ricky Gervais"
]
actors = [
  "Stephen Merchant",
  "Ricky Gervais",
  "Ashley Jensen",
  "Karl Pilkington",
  "Katherine Parkinson",
  "Keith Chegwin"
]
+++


One of my favourites. Extras tells a familiar story: Rags to Riches, and how the pursuit of fame and fortune can make a person forget what"s important along the way. That"s ok though, these stories are repeated through the generations for good reasons, as we need reminding.

The platonic friendship between Andy and Maggie is beautifully played, [Ricky](/pages/ricky-gervais) and [Ashley](/pages/ashley-jensen) have great chemistry and their people-watching and "What would you  rather be?" games are hilarious. [Stephen Merchant](/pages/stephen-merchant) is always fascinating to behold and he plays Andy"s agent brilliantly.

I"ve heard criticisms regarding all the star cameoes, something about Gervais just wanting to show off about being mates with famous people. Whether or not that"s true, they are all good fun with everyone playing a caricature of themselves and being willing to have a laugh at their own expense.

I can and do re-watch these endlessly.

{{< spoiler >}}
One thing I love about both Extras and [The Office](/pages/the-office) is that you get closure and a feelgood ending.
{{< /spoiler >}}
