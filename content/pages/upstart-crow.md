+++
title = "Upstart Crow"
shows = ["Upstart Crow"]

writers = [
  "Ben Elton"
]
actors = [
  "David Mitchell",
  "Gemma Whelan",
  "Mark Heap",
  "Helen Monks",
  "Liza Tarbuck"
]
+++


Nowhere near enough people know about this series. I think it may be [Ben Elton](/pages/ben-elton)'s magnum opus. The writing is fantastic, with a weird interplay between florid Shakespearean and modern slang which for me works really well ([Helen Monks](/pages/helen/monks), from [Raised by Wolves](/pages/raised-by-wolves) plays the stroppy teenage daughter beautifully). Episodes are loosely based on or refer to specific Shakespeare plays although a lot of the references go over the top of my head as I'm not very well educated.

[Mitchell](/pages/david-mitchell) is a perfect downtrodden Bard, the running gag being that he thinks he's a lot more clever than he really is and everyone around him repeatedly tells him to express himself more clearly, without the complex metaphors. [Mark Heap](/pages/mark-heap) is faultless (as always) as the arch-enemy Robert Greene, paying fastidious attention to delivery and really capturing an authentic feeling of the way some people might have spoken during Shakespeare's time (probably).

[Gemma Whelan](/pages/gemma-whelan) is brilliant as the anachronistic feminist, who dreams of being an actor and playing a woman (when everyone knows women are best played by men and the idea of a female actor is ridiculous).
